<?php
    require "translation.php";
    require "db.php";
    require "orders-db.php";

    $mysqli = getConnection();
    $order = getOrder($mysqli, $_GET["orderId"]);
    $clientEmail = $_GET["clientEmail"];
    $orderEmail = $order["client_email"];
    $mysqli->close();
    $orderState = $order["state"];
    $orderStateText = translate("order-status-{$order["state"]}");
    $gameText = translate("game-{$order["game"]}");
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Google tag (gtag.js) --> <script async src="https://www.googletagmanager.com/gtag/js?id=AW-923155910"></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'AW-923155910'); </script>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/index.css">
    <link rel="stylesheet" href="assets/css/header.css">
    <link rel="stylesheet" href="assets/css/footer.css">
    <link rel="stylesheet" href="assets/css/brands.min.css">
    <link rel="stylesheet" href="assets/css/solid.min.css">
    <link rel="stylesheet" href="assets/css/fontawesome.min.css">
    <link rel="stylesheet" href="assets/css/checkout.css">

    <title>Get Away Zone | Escape Room</title>

    <!-- Hotjar Tracking Code for GetAwayZone -->
    <script>
        (function(h,o,t,j,a,r) {
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:3475847,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,"https://static.hotjar.com/c/hotjar-",".js?sv=");
    </script>
</head>

<body>

<?php include "header.php"?>

<div class="d-flex container-checkout justify-content-center">
    <?php if ($clientEmail === $orderEmail && ($orderState === "CONFIRMED" || $orderState === "NEW")) { ?>
        <form method="post" action="order-actions.php">
            <input type="hidden" id="orderId" name="orderId" value="<?php echo $order['id'] ?>">
            <input type="hidden" id="clientEmail" name="clientEmail" value="<?php echo $clientEmail ?>">
            <input type="hidden" id="cancelOrderYouself" name="cancelOrderYouself" value="Y">
            <div class="form-group">
                <label for="game"><?php echo translate("form-element-game") ?></label>
                <input type="text" class="form-control form-control-sm" id="game" value="<?php echo $gameText ?>" readonly>
            </div>
            <div class="form-group">
                <label for="players"><?php echo translate("form-element-players") ?></label>
                <input type="text" class="form-control form-control-sm" id="players" value="<?php echo $order['players'] ?>"
                       readonly>
            </div>
            <div class="form-group">
                <label for="date_booked"><?php echo translate("form-element-date") ?></label>
                <input type="text" class="form-control form-control-sm" id="dateTimeBooked"
                       value="<?php echo $order["date_time_booked"] ?>" readonly>
            </div>
            <div class="form-group">
                <label for="price"><?php echo translate("form-element-price") ?>, &euro;</label>
                <input type="text" class="form-control form-control-sm" id="price" value="<?php echo $order['price'] ?>"
                       readonly>
            </div>
            <div class="form-group">
                <label for="client_name"><?php echo translate("form-element-name") ?></label>
                <input type="text" class="form-control form-control-sm" id="clientName"
                       value="<?php echo $order['client_name'] ?>" readonly>
            </div>
            <div class="form-group">
                <label for="client_email"><?php echo translate("form-element-email") ?></label>
                <input type="email" class="form-control form-control-sm" id="clientEmail"
                       value="<?php echo $order['client_email'] ?>" readonly>
            </div>
            <div class="form-group">
                <label for="client_phone"><?php echo translate("form-element-phone") ?></label>
                <input type="text" class="form-control form-control-sm" id="clientPhone"
                       value="<?php echo $order['client_phone'] ?>" readonly>
            </div>
            <div class="form-group">
                <label for="state"><?php echo translate("form-element-status") ?></label>
                <input type="text" class="form-control form-control-sm" id="state" value="<?php echo $orderStateText ?>" readonly>
            </div>
            <button type="submit" class="btn btn-sm btn-danger" name="btnCancel" value="Y"><?php echo translate("btn-order-cancel")?></button>
        </form>

    <?php } else {
        $text = translate("order-not-found");
        echo "<p>$text</p>";
    }?>
</div>

<?php include "footer.php"?>

</body>

</html>
