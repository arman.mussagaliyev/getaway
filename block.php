<?php

    require "db.php";
    require "orders-db.php";

    $timeSlotParams = array(
        "red-alert-10-00",
        "red-alert-11-20",
        "red-alert-12-40",
        "red-alert-14-00",
        "red-alert-15-20",
        "red-alert-16-40",
        "red-alert-18-00",
        "red-alert-19-20",
        "red-alert-20-40",
        "conjuring-10-15",
        "conjuring-11-35",
        "conjuring-12-55",
        "conjuring-14-15",
        "conjuring-15-35",
        "conjuring-16-55",
        "conjuring-18-15",
        "conjuring-19-35"
    );

    $cnt = count($timeSlotParams);
    $mysqli = getConnection();
    $date = $_GET["date-slot"];

    for ($i = 0; $i < $cnt; $i++) {
        $param = $timeSlotParams[$i];
        if (isset($_GET[$param]) || $_GET["blockOption"] === "whole") {
            $game = "";
            $time = str_replace("-", ":", substr($param, -5)) . ":00";
            if (substr($param, 0, 3) === "red") {
                $game = "red-alert";
            } elseif (substr($param, 0, 3) === "con") {
                $game = "conjuring";
            }
            $orderCreated = createOrder($mysqli, $game, 2, $date." ".$time, 0, "admin", "admin@admin.com", "+372777777777");
        }
    }
?>
