<?php

$translation = array(
    "our-email-en" => "info@getaway.zone",
    "our-email-et" => "info@getaway.zone",
    "our-email-ru" => "info@getaway.zone",

    "our-phone-en" => "+372 555 00 557",
    "our-phone-et" => "+372 555 09 509",
    "our-phone-ru" => "+372 555 00 557",

    "menu-the-conjuring-en" => "The Conjuring",
    "menu-red-alert-en" => "Red Alert",
    "menu-parties-en" => "Parties and Events",
    "menu-contacts-en" => "Contacts",

    "menu-the-conjuring-et" => "The Conjuring",
    "menu-red-alert-et" => "Red Alert",
    "menu-parties-et" => "Erapeod ja üritused",
    "menu-contacts-et" => "Kontakt",

    "menu-the-conjuring-ru" => "The Conjuring",
    "menu-red-alert-ru" => "Red Alert",
    "menu-parties-ru" => "Вечеринки",
    "menu-contacts-ru" => "Контакты",

    "main-description1-en" => "Ever wanted to challenge yourself and see how you will behave in a non-standard situation? Or maybe it’s your first time in Tallinn and you are looking for something exciting to do? Try to escape from one of the most popular, difficult and scariest escape room games in Tallinn.",
    "main-description1-et" => "",
    "main-description1-ru" => "Вам всегда хотелось бросить вызов себе и посмотреть, как вы будете вести себя в нестандартной ситуации? Или, может быть, вы впервые в Таллине и ищете что-нибудь интересное? Попробуйте сбежать из самых популярных, сложных и самых страшных квест комнат Таллинна.",

    "main-description2-en" => "These unique exit room games in the middle of the Old Town are suited for 2-6 players. You will have to solve the mystery to find the way out  before the door will be closed forever.",
    "main-description2-et" => "",
    "main-description2-ru" => "Эти уникальные игровые квест комнаты в центре Старого города рассчитаны на 2-6 игроков. Вам придется разгадать тайну, чтобы найти выход, прежде чем дверь закроется навсегда.",

    "main-description3-en" => "Our exit room games rely heavily on communication and teamwork. Every member of the team will have to play his own part: logic, arithmetic, perception, or even dexterity.",
    "main-description3-et" => "",
    "main-description3-ru" => "Наши квест комнаты в значительной степени зависят от общения и командной работы. Каждый участник команды проявит себя в лучшем:логика, арифметика, восприятие и даже ловкость.",

    "main-description4-en" => "Gather the clues, crack the code, and solve the riddle to Breakout before time runs out! Our “Escape Room” provides a fun, unique and immersive “Real Life Game” experience for friends, families, couples, groups, teams and coworkers.",
    "main-description4-et" => "Põgenemiseks koguge vihjeid, murdke lahti kood ja lahendage mõistatus enne, kui aeg otsa saab! Meie põgenemistuba on lõbus, ainulaadne ja paeluv interaktiivne seiklus, mis sobib nii sõpradele, peredele, paaridele, gruppidele, meeskondadele kui ka töökaaslastele.",
    "main-description4-ru" => "Соберите подсказки, взломайте коды и решите загадку, чтобы сбежать, прежде чем закончится время! Наша квест-комната обеспечит веселые, уникальные, реальные и захватывающие приключения для друзей, семей, влюбленных пар и коллег по работе.",

    "red-alert-description1-en" => "Estonia. Nowadays. You and your friends are members of a team of highly trained Western agents. Your mission is to break into the secret underground headquarters and successfully escape with the folder which contain classified information. You only have 61 minutes before the Base Commander, a KGB officer, gets back to his office. Our double agent successfully helped you to enter into the base and access the restricted.",
    "red-alert-description1-et" => "Teie meeskond kehastab vilunud Lääne salaagente. Teie missioon on tungida vaenlase põrandaalusesse peakorterisse, kätte saada toimik salastatud informatsiooniga ja edukalt põgeneda. Teil on aega vaid 61 minutit, enne kui baasiülem, kurikuulus KGB ohvitser, tagasi oma kabinetti saabub.",
    "red-alert-description1-ru" => "Действие событий происходит в 1985 году. Эстония – все еще часть Советского Союза. Вы и ваши друзья являются членами команды высококвалифицированных западных агентов. Ваша миссия – проникнуть в секретную подземную штаб-квартиру и сбежать прихватив папку с засекреченной информацией. У вас есть всего лишь 61 минута, прежде чем командир базы, офицер КГБ, вернется в свой кабинет.",

    "red-alert-description2-en" => "",
    "red-alert-description2-et" => "Meie topeltagent aitas teil edukalt baasi siseneda ja piiratud alale ligi pääseda. Ülesande täitmiseks peate sisse murdma ohvitseri kontorisse, tuvastama salastatud teavet sisaldava toimiku ja piiratud alast välja pääsema, et taaskohtuda topeltagendiga, kes teid ohutult KGB territooriumilt välja juhatab…",
    "red-alert-description2-ru" => "Наш двойной агент поможет вам попасть на базу и в зону ограниченного допуска. Вы должны пробраться в кабинет, обнаружить папку с засекреченной информацией, сбежать из зоны ограниченного доступа и присоединиться к двойному агенту, который поможет вам сбежать из штаб-квартиры КГБ…",

    "red-alert-description3-en" => "",
    "red-alert-description3-et" => "Pane ennast sellele ohtlikule missioonile kirja ja me saadame lõplikud juhiseid sinu e-posti aadressile…",
    "red-alert-description3-ru" => "Запишитесь на эту опасную миссию и получите более детальные инструкции по электронной почте…",

    "conjuring-description1-en" => "You are an investigative team from Vatican, here to look into reported supernatural activity. Upon entering the premises, you have disturbed a dormant poltergeist, who has closed down the building. According to psychometric readings the poltergeist is getting increasingly riled. You only have one hour before it evolves into a full-blown aetheric storm and destroys the location and you with it.",
    "conjuring-description1-et" => "Te olete Vatikani poolt saadetud uurimismeeskond, eesmärgiga mõista kas kuuldused siin toimuvatest üleloomulikest sündmustest peavad paika. Sisenedes suutsite ärritada senimaani vagusi olnud poltergeisti, kes nüüd on hoone väljapääsud sulgenud.",
    "conjuring-description1-ru" => "Вы делегация присланная из Ватикана.",

    "conjuring-description2-en" => "",
    "conjuring-description2-et" => "Psühhomeetriline analüüs viitab sellele, et poltergeist on oma pahandustega alles alustamas, ning tunni aja jooksul areneb see tavalisest poltergeistist millekski palju hullemaks, vaimutormiks mis hävitab nii hoone kui ka teid seal sees. Teie ainsaks lootuseks on lahendada siinne õõvastav saladus, seeläbi maha rahustada poltergeist ja põgeneda.",
    "conjuring-description2-ru" => "Вам предстоит обнаружить паранормальные явления и совершить оккультный обряд чистки/изгнания.",

    "read-more-en" => "Read more...",
    "read-more-et" => "Loe rohkem…",
    "read-more-ru" => "Узнать больше...",

    "party-section-header-en" => "Parties & Events",
    "party-section-header-et" => "Erapeod ja üritused",
    "party-section-header-ru" => "Частные вечеринки и мероприятия",

    "party-description1-en" => "Planning a celebration? We’ve got just the place. Whether you fancy a cocktail soiree for 50, or a more intimate three-course meal, our private function room in Vaike Karja is perfect for any kind of private events.",
    "party-description1-et" => "Planeerid pidu? Meil on Sinu jaoks õige koht. Pole vahet, kas eelistad uhket kokteiliõhtut 50-le inimesele või intiimset kolmekäigulist einet, meie privaatne Väike Karja peoruum on ideaalne kõigi sündmuste jaoks.",
    "party-description1-ru" => "Планируете празднование? Тогда это место именно для вас. Хотите ли вы устроить коктейльную вечеринку на 50 персон или более интимный обед из трех блюд — наша частная функциональная комната на улице Вяйке-Карья идеально подходит для проведения любых частных мероприятий.",

    "party-description2-en" => "It’s like having your very own pub for the day/night. The warming and cozy furnishings are comfortable and welcoming, but it’s also clean and contemporary, complete with its own bar and dedicated bartender. We can help you throw all types of private parties in Tallinn: Birthday Parties, Office Celebrations, Loft Parties, Anniversary, Bachelor & Hen Parties, Corporate Events, Retirement Parties, Main Bar Specials, Team Building Sessions and many more… No party is too big or too small. Our event planners are ready to help you organize everything: decorations, music, games and entertainment, food and beverage packages. And guess what? Our service is easy, professional, and really affordable! Contact us for a custom quote.",
    "party-description2-et" => "Terve päeva/öö võid tunda ennast tõelise pubiomanikuna. Koha interjöör on hubane ja kutsuv, sisustus mugav. Kõik on puhas ja kaasaegne. Sind ootab Sinu oma baar koos pühendunud baarmeniga.",
    "party-description2-ru" => "Это как иметь свой собственный паб на день/ночь. Теплая и уютная обстановка, чистота и современный дизайн, а также собственный бар с персональным барменом.",

    "party-description3-en" => "",
    "party-description3-et" => "Me saame Sind aidata igat tüüpi erapidude korraldamisega Tallinnas. Sinna hulka kuuluvad: sünnipäevapeod, tööüritused, aastapäevapidustused, poissmeeste- ja tüdrukuteõhtud, esindusüritused, ametist lahkumise peod, baari eripakkumised, meeskonnatöö arenguseminarid ja veel palju muudki.",
    "party-description3-ru" => "Мы с радостью поможем вам с организацией любой частной вечеринки в Таллине: дни рождения, офисные мероприятия, вечеринки в лофте, годовщины, вечеринки по поводу окончания института, девичники, корпоративные события, вечеринки по поводу ухода на пенсию, тренинги по построению командной работы и многое другое…",

    "party-description4-en" => "",
    "party-description4-et" => "Ükski pidu ei ole liiga suur või liiga väike. Meie üritustekorraldajad on valmis planeerima kõike: kaunistusi, muusikat, mänge ja meelelahutust, toidu- ja joogipakette.",
    "party-description4-ru" => "Ни одно мероприятие не является слишком большим или слишком маленьким для нас. Наши организаторы с радостью помогут вам все организовать: декорации, музыку, игры и развлечения, еду и напитки.",

    "contacts-section-header-en" => "Contacts",
    "contacts-section-header-et" => "Kontakt",
    "contacts-section-header-ru" => "Контакты",

    "game-page-conjuring-description1-en" => "You are an investigative team from Vatican, here to look into reported supernatural activity. Upon entering the premises, you have disturbed a dormant poltergeist, who has closed down the building.",
    "game-page-conjuring-description1-et" => "Te olete Vatikani poolt saadetud uurimismeeskond, eesmärgiga mõista kas kuuldused siin toimuvatest üleloomulikest sündmustest peavad paika. Sisenedes suutsite ärritada senimaani vagusi olnud poltergeisti, kes nüüd on hoone väljapääsud sulgenud.",
    "game-page-conjuring-description1-ru" => "Вы делегация присланная из Ватикана. Вам предстоит обнаружить паранормальные явления и совершить оккультный обряд чистки/изгнания.",

    "game-page-conjuring-description2-en" => "According to psychometric readings the poltergeist is getting increasingly riled. You only have one hour before it evolves into a full-blown aetheric storm and destroys the location and you with it. You need to solve the sinister mystery and help the poltergeist find peace in the afterlife to be able to escape alive",
    "game-page-conjuring-description2-et" => "Psühhomeetriline analüüs viitab sellele, et poltergeist on oma pahandustega alles alustamas, ning tunni aja jooksul areneb see tavalisest poltergeistist millekski palju hullemaks, vaimutormiks mis hävitab nii hoone kui ka teid seal sees. Teie ainsaks lootuseks on lahendada siinne õõvastav saladus, seeläbi maha rahustada poltergeist ja põgeneda.",
    "game-page-conjuring-description2-ru" => "",

    "game-page-red-alert-description1-en" => "Estonia. Nowadays. You and your friends are members of a team of highly trained Western agents. Your mission is to break into the secret underground headquarters and successfully escape with the folder which contain classified information. You only have 61 minutes before the Base Commander, a KGB officer, gets back to his office.",
    "game-page-red-alert-description1-et" => "Teie meeskond kehastab vilunud Lääne salaagente. Teie missioon on tungida vaenlase põrandaalusesse peakorterisse, kätte saada toimik salastatud informatsiooniga ja edukalt põgeneda. Teil on aega vaid 61 minutit, enne kui baasiülem, kurikuulus KGB ohvitser, tagasi oma kabinetti saabub.",
    "game-page-red-alert-description1-ru" => "Действие событий происходит в 1985 году. Эстония – все еще часть Советского Союза. Вы и ваши друзья являются членами команды высококвалифицированных западных агентов. Ваша миссия – проникнуть в секретную подземную штаб-квартиру и сбежать прихватив папку с засекреченной информацией. У вас есть всего лишь 61 минута, прежде чем командир базы, офицер КГБ, вернется в свой кабинет.",

    "game-page-red-alert-description2-en" => "Our double agent successfully helped you to enter into the base and access the restricted area. You must break inside the inner office, locate the folder which contain classified information and escape outside the restricted area so you can rejoin the double agent, who will then safely escort you outside the KGB premises…",
    "game-page-red-alert-description2-et" => "Meie topeltagent aitas teil edukalt baasi siseneda ja piiratud alale ligi pääseda. Ülesande täitmiseks peate sisse murdma ohvitseri kontorisse, tuvastama salastatud teavet sisaldava toimiku ja piiratud alast välja pääsema, et taaskohtuda topeltagendiga, kes teid ohutult KGB territooriumilt välja juhatab…",
    "game-page-red-alert-description2-ru" => "Наш двойной агент поможет вам попасть на базу и в зону ограниченного допуска. Вы должны пробраться в кабинет, обнаружить папку с засекреченной информацией, сбежать из зоны ограниченного доступа и присоединиться к двойному агенту, который поможет вам сбежать из штаб-квартиры КГБ…",

    "game-page-red-alert-description3-en" => "Enroll for this dangerous mission so to receive the final instructions in your email…",
    "game-page-red-alert-description3-et" => "Pane ennast sellele ohtlikule missioonile kirja ja me saadame lõplikud juhiseid sinu e-posti aadressile…",
    "game-page-red-alert-description3-ru" => "Запишитесь на эту опасную миссию и получите более детальные инструкции по электронной почте…",

    "book-now-en" => "Book now",
    "book-now-et" => "Broneeri kohe",
    "book-now-ru" => "Зарезервировать",

    "players-en" => "Players",
    "players-et" => "Mängijad",
    "players-ru" => "Игроки",

    "checkout-en" => "Checkout",
    "checkout-et" => "Broneerima",
    "checkout-ru" => "Оформить бронь",

    "placeholder-name-en" => "Your name",
    "placeholder-name-et" => "Oma nimi",
    "placeholder-name-ru" => "Ваше имя",

    "placeholder-email-en" => "E-mail address",
    "placeholder-email-et" => "E-post aadress",
    "placeholder-email-ru" => "E-mail address",

    "placeholder-phone-en" => "Phone",
    "placeholder-phone-et" => "Telefon",
    "placeholder-phone-ru" => "Телефон",

    "btn-confirm-en" => "Confirm",
    "btn-confirm-et" => "Kinnitage",
    "btn-confirm-ru" => "Подтвердить",

    "form-element-game-en" => "Room",
    "form-element-game-et" => "Ruumi",
    "form-element-game-ru" => "Комната",

    "form-element-players-en" => "Players",
    "form-element-players-et" => "Mängijad",
    "form-element-players-ru" => "Игроки",

    "form-element-date-en" => "Date",
    "form-element-date-et" => "Kuupäev",
    "form-element-date-ru" => "Дата",

    "form-element-time-en" => "Time",
    "form-element-time-et" => "Aeg",
    "form-element-time-ru" => "Время",

    "form-element-price-en" => "Price",
    "form-element-price-et" => "Hind",
    "form-element-price-ru" => "Цена",

    "form-element-name-en" => "Name",
    "form-element-name-et" => "Nimi",
    "form-element-name-ru" => "Имя",

    "form-element-email-en" => "e-mail",
    "form-element-email-et" => "e-post",
    "form-element-email-ru" => "e-mail",

    "form-element-phone-en" => "Phone",
    "form-element-phone-et" => "Telefon",
    "form-element-phone-ru" => "Телефон",

    "form-element-status-en" => "Status",
    "form-element-status-et" => "Status",
    "form-element-status-ru" => "Статус",

    "game-conjuring-en" => "Conjuring",
    "game-conjuring-et" => "Conjuring",
    "game-conjuring-ru" => "Conjuring",

    "game-red-alert-en" => "Red Alert",
    "game-red-alert-et" => "Red Alert",
    "game-red-alert-ru" => "Red Alert",

    "order-table-header-game-en" => "Room",
    "order-table-header-game-et" => "Ruumi",
    "order-table-header-game-ru" => "Комната",

    "order-table-header-players-en" => "Players",
    "order-table-header-players-et" => "Mängijad",
    "order-table-header-players-ru" => "Игроки",

    "order-table-header-date_and_time-en" => "Date & Time",
    "order-table-header-date_and_time-et" => "Kuupäev & Aeg",
    "order-table-header-date_and_time-ru" => "Дата & Время",

    "order-table-header-status-en" => "Status",
    "order-table-header-status-et" => "Staatus",
    "order-table-header-status-ru" => "Статус",

    "order-table-header-details-en" => "Details",
    "order-table-header-details-et" => "Üksikasjad",
    "order-table-header-details-ru" => "Детали",

    "order-status-new-en" => "New",
    "order-status-new-et" => "Uus",
    "order-status-new-ru" => "Новый",

    "order-status-confirmed-en" => "Confirmed",
    "order-status-confirmed-et" => "Kinnitas",
    "order-status-confirmed-ru" => "Подтвержден",

    "order-status-cancelled-en" => "Cancelled",
    "order-status-cancelled-et" => "Tühistatud",
    "order-status-cancelled-ru" => "Отменен",

    "btn-order-view-en" => "View",
    "btn-order-view-et" => "Vaatama",
    "btn-order-view-ru" => "Смотреть",

    "btn-order-confirm-en" => "Confirm order",
    "btn-order-confirm-et" => "Kinnita tellimus",
    "btn-order-confirm-ru" => "Подтвердить заказ",

    "btn-order-cancel-en" => "Cancel order",
    "btn-order-cancel-et" => "Tühista tellimus",
    "btn-order-cancel-ru" => "Отменить заказ",

    "booking-received-message-on-screen-en" => "Thank you {name}, your booking for {game} on {date} at {time} is received. Confirmation email will be sent to the email address {email} you provided.",
    "booking-received-message-on-screen-et" => "Aitäh {name}, Teie {game} reserveering  {date} kell {time} on kätte saadud. Kinnituskiri saadetakse Teie sisestatud e-posti aadressile {email}.",
    "booking-received-message-on-screen-ru" => "Спасибо {name}, ваша бронь на квест {game} {date} в {time} принята. Подтверждение будет отправлено на указанный вами адрес электронной почты {email}",

    "could-not-book-message-on-screen-en" => "Sorry, we could not book your time. The GetAway Zone team is notified about the problem. Please try again.",
    "could-not-book-message-on-screen-et" => "Vabandust, me ei saanud teie broneeritud aega kinnitada. Palun valige uus aeg.",
    "could-not-book-message-on-screen-ru" => "Ваша сессия устарела Пожалуйста, закройте это окно и попробуйте перейти на сайт заново",

    "booking-received-subject-en" => "Booking Received - {game}",
    "booking-received-subject-et" => "Broneering kätte saadud - {game}",
    "booking-received-subject-ru" => "Бронирование получено - {game}",

    "booking-confirmed-subject-en" => "Booking Confirmed - {game}",
    "booking-confirmed-subject-et" => "Broneering kinnitatud- {game}",
    "booking-confirmed-subject-ru" => "Бронирование подтверждено - {game}",

    "booking-cancelled-subject-en" => "{game} Booking Cancelled",
    "booking-cancelled-subject-et" => "{game} Broneering tühistatud",
    "booking-cancelled-subject-ru" => "{game} Бронирование отменено.",

    "booking-received-message-en" => "Dear {name}, your booking for {game} on {date} at {time} is received.",
    "booking-received-message-et" => "Aitäh {name}, Teie {game} reserveering  {date} kell {time} on kätte saadud.",
    "booking-received-message-ru" => "Спасибо {name}, ваша бронь на квест {game} {date} в {time} принята.",

    "booking-confirmed-message-en" => "Dear {name}, your booking for {game} on {date} at {time} has been confirmed.\nTo cancel your booking please use following link.\n{protocol}://{host}/order-cancel.php?clientEmail={email}&orderId={orderId}",
    "booking-confirmed-message-et" => "Lugupeetud {name}, Teie {game} broneering  {date} kell {time} on kinnitatud. Broneeringu tühistamiseks kasutage palun järgmist linki.\n{protocol}://{host}/order-cancel.php?clientEmail={email}&orderId={orderId}",
    "booking-confirmed-message-ru" => "Уважаемый {name},ваша бронь на квест {game} {date} начало в {time} подтверждена.\n Вы можете отменить бронь по ссылке.\n{protocol}://{host}/order-cancel.php?clientEmail={email}&orderId={orderId}",

    "booking-cancelled-message-en" => "Dear {name}, unfortunately your booking for {game} on {date} at {time} has been cancelled",
    "booking-cancelled-message-et" => "Lugupeetud {name}, kahjuks Teie {game} broneering  {date} kell {time} on tühistatud.",
    "booking-cancelled-message-ru" => "Уважаемый {name}, к сожалению квест {game} {date} в {time} был отменен.",

    "order-created-subject-en" => "Booking for {game} on {date} at {time}",
    "order-created-subject-et" => "Booking for {game} on {date} at {time}",
    "order-created-subject-ru" => "Booking for {game} on {date} at {time}",

    "order-created-message-en" => "Game: {game}\nPlayers: {players}\nDate Time Booked: {date} {time}\nPrice: {price}\nClient Name: {name}\nClient email: {email}\nClient Phone: {phone}",
    "order-created-message-et" => "Game: {game}\nPlayers: {players}\nDate Time Booked: {date} {time}\nPrice: {price}\nClient Name: {name}\nClient email: {email}\nClient Phone: {phone}",
    "order-created-message-ru" => "Game: {game}\nPlayers: {players}\nDate Time Booked: {date} {time}\nPrice: {price}\nClient Name: {name}\nClient email: {email}\nClient Phone: {phone}",

    "time-is-not-available-on-screen-message-en" => "Sorry, time is not available. Please choose another time",
    "time-is-not-available-on-screen-message-et" => "Vabandust, me ei saanud teie broneeritud aega kinnitada. Palun valige uus aeg.",
    "time-is-not-available-on-screen-message-ru" => "К сожалению выбранное время недоступно. Выберите пожалуйста другое время",

    "order-not-found-en" => "Order has been cancelled or does not exist",
    "order-not-found-et" => "Tellimus on tühistatud või ei eksisteeri",
    "order-not-found-ru" => "Ваша бронь отменена или отсутствует.",

    "order-admin-link-en" => "{protocol}://{host}/order-details.php?orderId={orderId}&lang=en",
    "order-admin-link-et" => "{protocol}://{host}/order-details.php?orderId={orderId}&lang=en",
    "order-admin-link-ru" => "{protocol}://{host}/order-details.php?orderId={orderId}&lang=en",

    "orders-list-en" => "Back to orders list",
    "orders-list-et" => "Tellimuste loetelu",
    "orders-list-ru" => "Назад к списку заказов",

    "email-html-en" => "  
        <html lang=\"en\">
            <head>
                <title>Get Away Zone Booking Notification</title>
                <style>
                    .message {
                    }
                </style>
            </head>
            <body>
                <p class=\"message\">{message-to-client}</p>
                <img src=\"https://boot.getaway.zone/assets/img/{game-code}.jpg\"/>
            </body>
        </html>
        ",

    "email-html-et" => "
        <html lang=\"et\">
            <head>
                <title>Get Away Zone Booking Notification</title>
                <style>
                    .message {
                    }
                </style>
            </head>
            <body>
                <p class=\"message\">{message-to-client}</p>
                <img src=\"https://boot.getaway.zone/assets/img/{game-code}.jpg\"/>
            </body>
        </html>
        ",

    "email-html-ru" => "
        <html lang=\"ru\">
            <head>
                <title>Get Away Zone Booking Notification</title>
                <style>
                    .message {
                    }
                </style>
            </head>
            <body>
                <p class=\"message\">{message-to-client}</p>
                <img src=\"https://boot.getaway.zone/assets/img/{game-code}.jpg\"/>
            </body>
        </html>
    ",
 );

if (!function_exists('translate')) {
    function translate($key)
    {
        $lang = "et";

        if (isset($_GET["lang"]))
            $lang = $_GET["lang"];
        elseif (isset($_POST["lang"]))
            $lang = $_POST["lang"];

        return translateToLang($key, $lang);
    }
}

if (!function_exists('translateToLang')) {
    function translateToLang($key, $lang)
    {

        global $translation;

        $result = "{$key}-{$lang}";

        if (isset($translation[strtolower("{$key}-{$lang}")])) {
            $result = $translation[strtolower("{$key}-{$lang}")];
        }

        return $result;
    }
}
