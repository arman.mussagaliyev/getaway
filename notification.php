<?php
    if (!function_exists("sendOrderNotification")) {
        function sendOrderNotification($msqli, $orderId, $subjectKey, $messageKey) {
            $order = getOrder($msqli, $orderId);
            $subject = fillTemplateSubstitutes(translateToLang($subjectKey, $order["lang"]), $order);
            $message = fillTemplateSubstitutes(translateToLang($messageKey, $order["lang"]), $order);
            $html = translateToLang("email-html", $order["lang"]);
            $html = str_replace("{game-code}", $order["game"], $html);
            $html = str_replace("{message-to-client}", $message, $html);

            $headers  = array(
                "MIME-Version" => "1.0",
                "Content-type" => "text/html; charset=iso-8859-1",
                "From" => "Get Away Zone <info@getaway.zone>",
                "Reply-To" => "info@getaway.zone",
                "X-Mailer" => "PHP/" . phpversion());

            mail($order["client_email"], $subject, $html, $headers);
        }
    }

    if (!function_exists("fillTemplateSubstitutes")) {
        function fillTemplateSubstitutes($template, $order)
        {
            $protocol = getProtocol();
            $host = getHost();
            $dateTime = strtotime($order["date_time_booked"]);

            $result = str_replace('{game}', translate("game-" . $order["game"]),
                str_replace('{name}', $order["client_name"],
                    str_replace('{date}', date('Y-m-d', $dateTime),
                        str_replace('{time}', date('H:i', $dateTime),
                            str_replace('{email}', $order["client_email"],
                                str_replace('{players}', $order["players"],
                                    str_replace('{phone}', $order["client_phone"],
                                        str_replace('{orderId}', $order["id"],
                                            str_replace('{protocol}', $protocol,
                                                str_replace('{host}', $host,
                                                    str_replace('{price}', $order["price"], $template)))))))))));

            return $result;
        }
    }

    if (!function_exists("sendAdminNotification")) {
        function sendAdminNotification($subject, $message)
        {
            mail("info@getaway.zone", $subject, $message, "From: GAZ Booking <booking@getaway.zone>");
        }
    }

    if (!function_exists("getProtocol")) {
        function getProtocol()
        {
            if ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443)
                return "https";
            else
                return "http";
        }
    }

    if (!function_exists("getHost")) {
        function getHost()
        {
            return $_SERVER["HTTP_HOST"];
        }
    }
?>
