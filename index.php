<?php
    require "translation.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Google tag (gtag.js) --> <script async src="https://www.googletagmanager.com/gtag/js?id=AW-923155910"></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'AW-923155910'); </script>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" type="image/png" href="/favicon.png">

    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/index.css">
	<link rel="stylesheet" href="assets/css/header.css">
	<link rel="stylesheet" href="assets/css/footer.css">
    <link rel="stylesheet" href="assets/css/brands.min.css">
    <link rel="stylesheet" href="assets/css/solid.min.css">
    <link rel="stylesheet" href="assets/css/fontawesome.min.css">

    <title>Get Away Zone | Escape Room</title>

    <!-- Hotjar Tracking Code for GetAwayZone -->
    <script>
        (function(h,o,t,j,a,r) {
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:3475847,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,"https://static.hotjar.com/c/hotjar-",".js?sv=");
    </script>
</head>

<body>

    <?php include "header.php"?>

    <div id="container-room" class="d-flex flex-column">
        <h1 class="text-center">GET AWAY ZONE</h1>

        <div class="d-flex flex-row mb-1">
            <div class="p-2 flex-fill d-flex justify-content-center">
                <p id="descr-1" class="text-left">
                    <?php echo translate("main-description1")?>
                </p>

                <p id="descr-2" class="text-left">
                    <?php echo translate("main-description2")?>
                </p>

                <p id="descr-3" class="text-left">
                    <?php echo translate("main-description3")?>
                </p>

                <p id="descr-4" class="text-left">
                    <?php echo translate("main-description4")?>
                </p>
            </div>
        </div>
        <div class="d-flex flex-row mb-2">
            <div class="p-2 flex-fill d-flex justify-content-end">
                <a href="conjuring.php?lang=<?php echo getCurrentLang()?>" id="btn-conjuring" class="btn btn-sm active btn-game" role="button" aria-pressed="true">THE CONJURING</a>
            </div>
            <div class="p-2 flex-fill d-flex justify-content-start">
                <a href="red-alert.php?lang=<?php echo getCurrentLang()?>" id="btn-red-alert" class="btn btn-sm active btn-game" role="button" aria-pressed="true">RED ALERT</a>
            </div>
        </div>
    </div>

    <div id="container-red-alert" class="container-fluid">
        <div class="row">
            <div id="container-red-alert-left" class="col-sm-6">
                <img src="assets/img/red-alert.jpg" alt="">
            </div>
            <div id="container-red-alert-right" class="col-sm-6">
                <div class="d-flex align-items-center">
                    <div class="text-start">
                        <h4>Red Alert</h4>
                        <p>
                            <?php echo translate("red-alert-description1")?>
                        </p>
                        <p>
                            <?php echo translate("red-alert-description2")?>
                        </p>
                        <p>
                            <?php echo translate("red-alert-description3")?>
                        </p>
                        <p class="text-right">
                            <a href="red-alert.php?lang=<?php echo getCurrentLang()?>"><?php echo translate("read-more")?></a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="container-conjuring" class="container-fluid">
        <div class="row">
            <div id="container-conjuring-left" class="col-sm-6">
                <div class="d-flex align-items-center">
                    <div class="text-start">
                        <h4>The Conjuring</h4>
                        <p>
                            <?php echo translate("conjuring-description1")?>
                        </p>
                        <p>
                            <?php echo translate("conjuring-description2")?>
                        </p>
                        <p class="text-right">
                            <a href="conjuring.php?lang=<?php echo getCurrentLang()?>"><?php echo translate("read-more")?></a>
                        </p>
                    </div>
                </div>
            </div>
            <div id="container-conjuring-right" class="col-sm-6">
                <img src="assets/img/conjuring.jpg" alt="">
            </div>
        </div>
    </div>

    <div id="container-parties" class="d-flex flex-column">
        <h1 class="text-center"><?php echo translate("party-section-header")?></h1>

        <div class="d-flex flex-row mb-1">
            <div class="p-2 flex-fill d-flex justify-content-center">
                <p class="text-left">
                    <?php echo translate("party-description1")?>
                </p>

            </div>
        </div>
        <div class="d-flex flex-row mb-1">
            <div class="p-2 flex-fill d-flex justify-content-center">
                <div id="container-gallery" class="container-fluid">
                    <div class="row">
                        <div class="col-sm-3">
                            <a href="assets/img/gallery-parties/gallery-6.jpg" title="gallery-6">
                                <img src="assets/img/gallery-parties/party-6-400x284.jpg" alt="">
                            </a>
                        </div>
                        <div class="col-sm-3">
                            <a href="assets/img/gallery-parties/gallery-5.jpg" title="gallery-5">
                                <img src="assets/img/gallery-parties/party-5-400x284.jpg" alt="">
                            </a>
                        </div>
                        <div class="col-sm-3">
                            <a href="assets/img/gallery-parties/gallery-4.jpg" title="gallery-4">
                                <img src="assets/img/gallery-parties/party-4-400x284.jpg" alt="">
                            </a>
                        </div>
                        <div class="col-sm-3">
                            <a href="assets/img/gallery-parties/gallery-3.jpg" title="gallery-3">
                                <img src="assets/img/gallery-parties/party-3-400x284.jpg" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <a href="assets/img/gallery-parties/gallery-2.jpg" title="gallery-2">
                                <img src="assets/img/gallery-parties/party-2-400x284.jpg" alt="">
                            </a>
                        </div>
                        <div class="col-sm-3">
                            <a href="assets/img/gallery-parties/gallery-1.jpg" title="gallery-1">
                                <img src="assets/img/gallery-parties/party-1-400x284.jpg" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="d-flex flex-row mb-1">
            <div class="p-2 flex-fill d-flex justify-content-center">
                <p class="text-left">
                    <?php echo translate("party-description2")?>
                </p>
            </div>
        </div>
        <div class="d-flex flex-row mb-1">
            <div class="p-2 flex-fill d-flex justify-content-center">
                <p class="text-left">
                    <?php echo translate("party-description3")?>
                </p>
            </div>
        </div>
        <div class="d-flex flex-row mb-1">
            <div class="p-2 flex-fill d-flex justify-content-center">
                <p class="text-left">
                    <?php echo translate("party-description4")?>
                </p>
            </div>
        </div>
    </div>


    <?php include "footer.php"?>

</body>

</html>
