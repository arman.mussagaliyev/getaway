const orders = (function($) {

    function getOrders() {
        $("#ordersForm").submit();
    }

    return {
        getOrders: getOrders
    };

})(jQuery);
