let booking = (function($) {

    const defaultPrice = 60;

    let bookingData = {
        players: 2,
        date: undefined,
        weekday: undefined,
        time: undefined,
        price: undefined,
        game: $("#game").val(),
        lang: 'et'
    };

    const classToPriceAndTimeMap = {
        "red-alert": {
            "available-cell-1": {
                "time": "11:30",
                "days": [0, 1, 2, 3, 4, 5, 6]
            },
            "available-cell-2": {
                "time": "12:45",
                "days": [0, 1, 2, 3, 4, 5, 6]
            },
            "available-cell-3": {
                "time": "14:00",
                "days": [0, 1, 2, 3, 4, 5, 6]
            },
            "available-cell-4": {
                "time": "15:15",
                "days": [0, 1, 2, 3, 4, 5, 6]
            },
            "available-cell-5": {
                "time": "16:30",
                "days": [0, 1, 2, 3, 4, 5, 6]
            },
            "available-cell-6": {
                "time": "17:45",
                "days": [0, 1, 2, 3, 4, 5, 6]
            },
            "available-cell-7": {
                "time": "19:00",
                "days": [0, 1, 2, 3, 4, 5, 6]
            },
            "available-cell-8": {
                "time": "20:15",
                "days": [5, 6]
            },
            "available-cell-9": {
                "time": "21:30",
                "days": [5, 6]
            },
            "available-cell-10": {
                "time": "22:30",
                "days": [5, 6]
            }
        },
        "conjuring": {
            "available-cell-1": {
                "time": "11:45",
                "days": [0, 1, 2, 3, 4, 5, 6]
            },
            "available-cell-2": {
                "time": "13:00",
                "days": [0, 1, 2, 3, 4, 5, 6]
            },
            "available-cell-3": {
                "time": "14:15",
                "days": [0, 1, 2, 3, 4, 5, 6]
            },
            "available-cell-4": {
                "time": "15:30",
                "days": [0, 1, 2, 3, 4, 5, 6]
            },
            "available-cell-5": {
                "time": "16:45",
                "days": [0, 1, 2, 3, 4, 5, 6]
            },
            "available-cell-6": {
                "time": "18:00",
                "days": [0, 1, 2, 3, 4, 5, 6]
            },
            "available-cell-7": {
                "time": "19:15",
                "days": [0, 1, 2, 3, 4, 5, 6]
            },
            "available-cell-8": {
                "time": "20:30",
                "days": [5, 6]
            },
            "available-cell-9": {
                "time": "21:30",
                "days": [5, 6]
            }
        }
    };

    let weeks = [1, 2, 3, 4, 5, 6];
    let days = [1, 2, 3, 4, 5, 6, 0];
    let weekRow = '<tr class="calendar-week-row">';

    function buildDayCell() {
        return $('<td class="text-center calendar-day-cell">');
    }

    function getPlayersRow(labelText) {
        return `
            <tr class="players-row">
                <td colspan="2">
                    <p id="players-label" class="text-left">${labelText}</p>
                </td>
                <td>
                    <button type="button" class="btn btn-sm" id="btn-remove-player">
                        <img src="/assets/img/bootstrap-icons/dash.svg" alt="">
                    </button>
                </td>
                <td>
                    <input type="text" class="text-center" id="players-input" readonly="readonly" value="2" size="2"/>
                </td>
                <td>
                    <button type="button" class="btn btn-sm" id="btn-add-player">
                        <img src="/assets/img/bootstrap-icons/plus.svg" alt="">
                    </button>
                </td>
                <td colspan="2"></td>
            </tr>
        `;
    }

    function getTimeSlotsRow(date) {
        let timeSlotsRow = '<tr class="time-slots-row">';
        let i = 1;
        for (const key in classToPriceAndTimeMap[bookingData.game]) {
            const days = classToPriceAndTimeMap[bookingData.game][key]['days'];
            if (!days.includes(date.getDay())) {
                continue;
            }
            if (i === 7) {
                timeSlotsRow += '</tr><tr class="time-slots-row">';
            }
            const time = classToPriceAndTimeMap[bookingData.game][key]['time'];
            timeSlotsRow += `
                <td class="time-slot-cell">
                    <div id="${key}" class="available-cell">
                        <p class="text-center time-text">${time}</p>
                        <p id="price-${i}" class="text-center price-text" time="${time}">price</p>
                    </div>
                </td>
        `;
            i++;
        }
        timeSlotsRow += "</tr>";
        return timeSlotsRow;
    }

    let currDate = new Date();

    function getCurrDate(){
        return new Date(currDate);
    }

    function setCurrDate(date){
        currDate = new Date(date);
    }

    function setMonthHeader(month) {
        $(".month").text(month);

    }
    function fillWeekDays(formatter) {

        let monday = getMonday();

        days.forEach(day => {
            $(`#week-day-${day}`).text(formatter.format(addDays(monday, day - 1)))
        })
    }

    function getNextMonthFirstDate(fromDate, months) {
        let result = new Date(fromDate);
        result.setMonth(fromDate.getMonth() + months);
        result.setDate(1);
        return result;
    }

    function getMonday() {
        let day = new Date().getDay();
        return addDays(new Date(), -day + (day === 0 ? -6 : 1));
    }

    function addDays(date, days) {
        let result = new Date(date);
        result.setDate(result.getDate() + days);
        return result;
    }

    function formatter(options) {
        return new Intl.DateTimeFormat(bookingData.lang, options);
    }

    function fillCalendar(fromDate, lang) {

        bookingData.lang = lang;

        let dayFormatter = formatter({
            day: "numeric"
        });

        let weekDayFormatter = formatter({
            weekday: "short"
        });

        let monthFormatter = formatter({
            month: "long"
        });

        let yearFormatter = formatter({
            year: "numeric"
        });

        let toDate = getNextMonthFirstDate(fromDate, 1);

        setMonthHeader(`${monthFormatter.format(fromDate)} ${yearFormatter.format(fromDate)}`);

        fillWeekDays(weekDayFormatter);

        let nextDate = fromDate;
        let startPopulation = false;

        $(".calendar-week-row").remove();
        $(".players-row").remove();
        $(".time-slots-row").remove();

        let today = new Date();

        weeks.forEach((week) => {

            let weekElement = $(weekRow);
            let weekHasDays = false;

            days.forEach(day => {
                let dayElement = buildDayCell();

                if (week === 1 && day === fromDate.getDay()) {
                    startPopulation = true;
                }

                if (startPopulation) {

                    if (week === 1 && day === fromDate.getDay()) {
                        nextDate = fromDate;
                    }

                    if (datesAreEqual(nextDate, today) || dateIsAfter(nextDate, today)) {
                        dayElement.append(`<p>${dayFormatter.format(nextDate)}</p>`);
                        dayElement.addClass("available-cell");
                        let id = nextDate.getTime();
                        dayElement.attr("id", id);
                        const newDate = new Date(nextDate);
                        dayElement.on("click", () => selectDate(id, newDate));
                        weekHasDays = true;
                    } else {
                        dayElement.addClass("calendar-disabled-day");
                    }
                } else {
                    dayElement.addClass("calendar-disabled-day");
                }

                nextDate = addDays(nextDate, 1);

                weekElement.append(dayElement);

                if (datesAreEqual(nextDate, toDate)) {
                    startPopulation = false;
                }

            });

            if (weekHasDays) {
                $(`.calendar`).append(weekElement);
            }
        });

        setCurrDate(fromDate);

    }

    function selectDate(id, date) {
        $.get(`/available-time-slots.php?game=${bookingData.game}&date=${dateToStr(date)}`, (resp) => {
            unselectDate();
            markDateSelected(id);
            showTimeSlots(date);
            setPrice();
            disableBookedTimeSlots(JSON.parse(resp));
            disablePassedTimeSlots(date);
            showPlayersRow();
            hideCheckoutButton();
            bookingData.date = date;
        });
    }

    function dateToStr(date) {
        const month = `${date.getMonth() + 1}`.padStart(2, "0");
        const day = `${date.getDate()}`.padStart(2, "0");
        return `${date.getFullYear()}-${month}-${day}`;
    }

    function unselectDate() {
        $("td.available-cell-selected").addClass("available-cell");
        $("td.available-cell-selected").removeClass("available-cell-selected");
    }

    function markDateSelected(id) {
        $(`#${id}`).removeClass("available-cell");
        $(`#${id}`).addClass("available-cell-selected");
    }

    function showTimeSlots(date) {
        $(".time-slots-row").remove();
        $(".available-time").append($(getTimeSlotsRow(date)));
    }

    function showPlayersRow() {
        $(".players-row").remove();
        $(".players").append($(getPlayersRow(translations["players-" + bookingData.lang])));
        $("#btn-remove-player").on("click", () => addPlayer(-1));
        $("#btn-add-player").on("click", () => addPlayer(1));
    }

    function disableBookedTimeSlots(bookedTimeSlots) {
        $(".time-slot-cell div").each((i, e) => {
            const id = $(e).attr("id");
            const timeSlot = classToPriceAndTimeMap[bookingData.game][id]["time"];
            if (bookedTimeSlots.filter(e => e === timeSlot).length > 0) {
                $(e).removeClass("available-cell");
                $(e).dis
            } else {
                $(e).on("click", () => selectTime(id));
            }
        });
    }

    function disablePassedTimeSlots(selectedDate) {
        const now = new Date();
        $(".time-slot-cell div").each((i, e) => {
            const latestAllowedBookingDateTime = getSelectedDateLatestAllowedBookingDateTime($(e).attr("id"), selectedDate);
            if (now.getTime() > latestAllowedBookingDateTime.getTime()) {
                $(e).removeClass("available-cell");
                $(e).prop("onclick", null).off("click");
            }
        });
    }

    function getSelectedDateLatestAllowedBookingDateTime(id, selectedDate) {
        const timeSlot = classToPriceAndTimeMap[bookingData.game][id]["time"];
        const [hours, minutes] = timeSlot.split(":");
        selectedDate.setHours(hours);
        selectedDate.setMinutes(minutes);
        selectedDate.setSeconds(0);
        return new Date(selectedDate.getTime() - 60 * 60000);
    }

    function selectTime(id) {
        $("div.available-cell-selected").addClass("available-cell");
        $("div.available-cell-selected").removeClass("available-cell-selected");
        $(`#${id}`).removeClass("available-cell");
        $(`#${id}`).addClass("available-cell-selected");
        setPrice();
        showCheckoutButton();
        bookingData.time = classToPriceAndTimeMap[bookingData.game][id]["time"];
    }

    function showCheckoutButton() {
        $(".checkout-button-row").remove();
        let checkoutButtonRow = `
            <tr class="checkout-button-row">
                <td colspan="7">
                    <button class="btn btn-lg checkout-button"></button>
                </td>
            </tr>
        `;

        $(".book-button").append($(checkoutButtonRow));
        $(".checkout-button").text(translations["checkout-" + bookingData.lang]);
        $(".checkout-button").on("click", () => checkout());

    }

    function hideCheckoutButton() {
        $(".checkout-button-row").remove();
    }

    function addPlayer(inc) {
        let players = parseInt($("#players-input").val());
        players += inc;

        if (players < 2 || players > 7) {
            return;
        }
        setPlayers(players);
    }

    function setPlayers(players) {
        bookingData.players = players;
        $("#players-input").val (players);
        setPrice(bookingData.players);
    }

    function setPrice() {
        $(".price-text").each((idx) => {
            const price = pricing.getPrice(bookingData.players);
            $(`#price-${idx + 1}`).text(price + "€");
            bookingData.price = price;
        });


    }

    function compareDates(date1, date2) {
        if (date1.getFullYear() === date2.getFullYear()) {
            if (date1.getMonth() === date2.getMonth()) {
                if (date1.getDate() === date2.getDate()) {
                    return 0;
                } else if (date1.getDate() > date2.getDate()) {
                    return 1;
                } else {
                    return -1
                }
            } else if (date1.getMonth() > date2.getMonth()) {
                return 1;
            } else {
                return -1
            }
        } else if (date1.getFullYear() > date2.getFullYear()) {
            return 1;
        } else {
            return -1
        }
    }

    function dateIsBefore(date1, date2) {
        return compareDates(date1, date2) === -1;
    }

    function dateIsAfter(date1, date2) {
        return compareDates(date1, date2) === 1;
    }

    function datesAreEqual(date1, date2) {
        return compareDates(date1, date2) === 0;
    }

    function checkout() {
        const dateStr = dateToStr(bookingData.date);
        window.location = `/checkout.php?date=${dateStr}&time=${bookingData.time}&price=${bookingData.price}&game=${bookingData.game}&lang=${bookingData.lang}&players=${bookingData.players}`;
    }

    return {
        fillCalendar: fillCalendar,
        getCurrDate: getCurrDate,
        setCurrDate: setCurrDate,
        getNextMonthFirstDate: getNextMonthFirstDate,
        bookingData: bookingData
    }
})(jQuery);
