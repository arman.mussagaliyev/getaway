const pricing = (function() {
    function getPrice(players) {
        if (players === 2) {
            return 60;
        } else if (players === 3) {
            return 70;
        } else if (players === 4) {
            return 80;
        } else if (players === 5) {
            return 85;
        } else if (players === 6) {
            return 90;
        } else {
            return 105;
        }
    }

    return {
        getPrice: getPrice
    };
})();
