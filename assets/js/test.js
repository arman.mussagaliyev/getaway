const games = ["red-alert"];
const days = [1,2,3,4,5,6,7];
const times = [10, 12,14,16,18,20,22];
const players=[2,3,4,5,6];
const prices = [0, 0, 55,65,70,75,80];

games.forEach(game => {
    days.forEach(day => {
        times.forEach(time => {
            players.forEach(player => {
                console.log(`${game} - ${day} - ${time} - ${player} - ${prices[player]}`);
            })
        })
    })
});
