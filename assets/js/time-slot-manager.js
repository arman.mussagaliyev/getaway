const timeSlotManager = (function($) {
    function setDefaultDate() {
        $("#dateSlot").value = (new Date()).toISOString().substr(0, 10);
    }

    function onBlockOptionChange() {
        if ($("input[name='blockOption']:checked").val() === "selected") {
            $(".time-slots-all").show();
        } else {
            $(".time-slots-all").hide();
        }
    }

    $("input[name='blockOption']").change(onBlockOptionChange);
    onBlockOptionChange();

    return {
        setDefaultDate: setDefaultDate,
        onBlockOptionChange: onBlockOptionChange
    }
})(jQuery);
