<?php

function getCurrentUrl() {
    if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')
        $url = "https";
    else
        $url = "http";

    $url .= "://";

    $url .= $_SERVER['HTTP_HOST'];

    if(isset($_SERVER['PORT']))
        $url = ":" . $_SERVER['PORT'];

    $url .= $_SERVER['REQUEST_URI'];

    return $url;
}

function getUrlWithLangParam($lang) {
    $url_parts = parse_url(getCurrentUrl());

    $params = array();

    if (isset($url_parts['query'])) {
        parse_str($url_parts['query'], $params);
    }

    $params['lang'] = $lang;

    $url_parts['query'] = http_build_query($params);

    return "{$url_parts['scheme']}://{$url_parts['host'] }" . (isset($url_parts['port']) ? ":{$url_parts['port']}" : "") . $url_parts['path'] . '?' . $url_parts['query'];
}

function isIndexPage() {
    return (strpos(getCurrentUrl(),"index.php") != false) ||
        (strpos(getCurrentUrl(),".php") == false);
}

function getCurrentLang() {
    $url_parts = parse_url(getCurrentUrl());

    $params = array();

    if (isset($url_parts['query'])) {
        parse_str($url_parts['query'], $params);
    }

    if (isset($params['lang']))
        return $params['lang'];
    else
        return "et";

}

function getLocalUrl($point) {
    if (isIndexPage())
        return $point;
    else {
        $currentLang = getCurrentLang();
        return "/?lang={$currentLang}{$point}";
    }
}


function getLangImg($lang) {
    if ($lang == "en") {
        return '
                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAflJREFUeNpinDRzn5qN3uFDt16+YWBg+Pv339+KGN0rbVP+//2rW5tf0Hfy/2+mr99+yKpyOl3Ydt8njEWIn8f9zj639NC7j78eP//8739GVUUhNUNuhl8//ysKeZrJ/v7z10Zb2PTQTIY1XZO2Xmfad+f7XgkXxuUrVB6cjPVXef78JyMjA8PFuwyX7gAZj97+T2e9o3d4BWNp84K1NzubTjAB3fH0+fv6N3qP/ir9bW6ozNQCijB8/8zw/TuQ7r4/ndvN5mZgkpPXiis3Pv34+ZPh5t23//79Rwehof/9/NDEgMrOXHvJcrllgpoRN8PFOwy/fzP8+gUlgZI/f/5xcPj/69e/37//AUX+/mXRkN555gsOG2xt/5hZQMwF4r9///75++f3nz8nr75gSms82jfvQnT6zqvXPjC8e/srJQHo9P9fvwNtAHmG4f8zZ6dDc3bIyM2LTNlsbtfM9OPHH3FhtqUz3eXX9H+cOy9ZMB2o6t/Pn0DHMPz/b+2wXGTvPlPGFxdcD+mZyjP8+8MUE6sa7a/xo6Pykn1s4zdzIZ6///8zMGpKM2pKAB0jqy4UE7/msKat6Jw5mafrsxNtWZ6/fjvNLW29qv25pQd///n+5+/fxDDVbcc//P/zx/36m5Ub9zL8+7t66yEROcHK7q5bldMBAgwADcRBCuVLfoEAAAAASUVORK5CYII=" title="English" alt="English" width="16" height="11">
                English
        ';
    } else if ($lang == "et") {
        return '
                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAE/SURBVHjaYmRw3s8AB/8YGH79Y/gDRr/+gNhA9OMPmP2H4cc/ht9/AAJQKMcEAIAwDMB2YAV5U4ArNOCqa8v25MrqdnJPVsim3JlS0cU2MAoy6JvvCyAWiNFP3gPFQaqB1N+/IBIo/fsPiPz1598vIPf3P1lhdgaGPwABxAJ0xj+QkUCDQaYCVf8CGQlSDVIHJP+A9Pz+B9QMdPEfgABiYdjv+Ne07M+jR3+A4DcIQsCvX2ACRoGE5OUZGA4CBBDISX/+/AVy4Rqg0sjKwTRQHqgYIIBAGv7+BSn+9fsXzHyQAmSz4UYAFQMEEOPTp0/FxcX///8P5PzHDUBKGRkvXLgAEEAs/4BeBislqAEc9P8AAgikAa4aHoFAwxhQAJD7H6IBIIBAGh48eACyBwyBnvkHBhAGxKP/YADIBggwANQufux425lWAAAAAElFTkSuQmCC" title="Estonia" alt="Estonia" width="16" height="11">
                Eesti
        ';
    } else if ($lang == "ru") {
        return '
                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAE2SURBVHjaYvz69T8DAvz79w9CQVj/0MCffwwAAcQClObiAin6/x+okxHMgPCAbOb//5n+I4EXL74ABBALxGSwagTjPzbAyMgItAQggBg9Pf9nZPx//x7kjL9////9C2QAyf9//qCQQCQkxFhY+BEggFi2b/+nq8v46BEDSPQ3w+8//3//BqFfv9BJeXmQEwACCOSkP38YgHy4Bog0RN0vIOMXVOTPH6Cv/gEEEEgDxFKgHEgDXCmGDUAE1AAQQCybGZg1f/d8//XsH0jTn3+///z79RtE/v4NZfz68xfI/vOX+4/0ZoZFAAHE4gYMvD+3/v2+h91wCANo9Z+/jH9VxBkYAAKIBRg9TL//MEhKAuWAogxgZzGC2CCfgUggAoYdGAEVAwQQ41egu5AQAyoXTQoIAAIMAD+JZR7YOGEWAAAAAElFTkSuQmCC" title="Русский" alt="Русский" width="16" height="11">
                Русский
        ';
    } else {
        getLangImg("et");
    }
}

?>

<script src="assets/js/jquery-3.4.1.min.js"></script>

<script type="text/javascript" src="translation-to-js.php"></script>

<header>
    <div class="d-flex">

        <nav id="nav-bar-main" class="navbar navbar-expand-lg navbar-dark bg-transparent">
            <a class="navbar-brand" href="/?lang=<?php echo getCurrentLang()?>">
                <img src="assets/img/logo.png" class="responsive" alt=""/>
            </a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="conjuring.php?lang=<?php echo getCurrentLang()?>"><?php echo translate("menu-the-conjuring") ?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="red-alert.php?lang=<?php echo getCurrentLang()?>"><?php echo translate("menu-red-alert") ?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo getLocalUrl('#container-parties')?>"><?php echo translate("menu-parties") ?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#container-contacts"><?php echo translate("menu-contacts") ?></a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="javascript:null" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <?php echo getLangImg(getCurrentLang())?>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="<?php echo  getUrlWithLangParam('et')?>">
                                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAE/SURBVHjaYmRw3s8AB/8YGH79Y/gDRr/+gNhA9OMPmP2H4cc/ht9/AAJQKMcEAIAwDMB2YAV5U4ArNOCqa8v25MrqdnJPVsim3JlS0cU2MAoy6JvvCyAWiNFP3gPFQaqB1N+/IBIo/fsPiPz1598vIPf3P1lhdgaGPwABxAJ0xj+QkUCDQaYCVf8CGQlSDVIHJP+A9Pz+B9QMdPEfgABiYdjv+Ne07M+jR3+A4DcIQsCvX2ACRoGE5OUZGA4CBBDISX/+/AVy4Rqg0sjKwTRQHqgYIIBAGv7+BSn+9fsXzHyQAmSz4UYAFQMEEOPTp0/FxcX///8P5PzHDUBKGRkvXLgAEEAs/4BeBislqAEc9P8AAgikAa4aHoFAwxhQAJD7H6IBIIBAGh48eACyBwyBnvkHBhAGxKP/YADIBggwANQufux425lWAAAAAElFTkSuQmCC" title="Estonia" alt="Estonia" width="16" height="11">
                                Eesti
                            </a>
                            <a class="dropdown-item" href="<?php echo getUrlWithLangParam('en')?>">
                                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAflJREFUeNpinDRzn5qN3uFDt16+YWBg+Pv339+KGN0rbVP+//2rW5tf0Hfy/2+mr99+yKpyOl3Ydt8njEWIn8f9zj639NC7j78eP//8739GVUUhNUNuhl8//ysKeZrJ/v7z10Zb2PTQTIY1XZO2Xmfad+f7XgkXxuUrVB6cjPVXef78JyMjA8PFuwyX7gAZj97+T2e9o3d4BWNp84K1NzubTjAB3fH0+fv6N3qP/ir9bW6ozNQCijB8/8zw/TuQ7r4/ndvN5mZgkpPXiis3Pv34+ZPh5t23//79Rwehof/9/NDEgMrOXHvJcrllgpoRN8PFOwy/fzP8+gUlgZI/f/5xcPj/69e/37//AUX+/mXRkN555gsOG2xt/5hZQMwF4r9///75++f3nz8nr75gSms82jfvQnT6zqvXPjC8e/srJQHo9P9fvwNtAHmG4f8zZ6dDc3bIyM2LTNlsbtfM9OPHH3FhtqUz3eXX9H+cOy9ZMB2o6t/Pn0DHMPz/b+2wXGTvPlPGFxdcD+mZyjP8+8MUE6sa7a/xo6Pykn1s4zdzIZ6///8zMGpKM2pKAB0jqy4UE7/msKat6Jw5mafrsxNtWZ6/fjvNLW29qv25pQd///n+5+/fxDDVbcc//P/zx/36m5Ub9zL8+7t66yEROcHK7q5bldMBAgwADcRBCuVLfoEAAAAASUVORK5CYII=" title="English" alt="English" width="16" height="11">
                                English
                            </a>
                            <a class="dropdown-item" href="<?php echo getUrlWithLangParam('ru')?>">
                                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAE2SURBVHjaYvz69T8DAvz79w9CQVj/0MCffwwAAcQClObiAin6/x+okxHMgPCAbOb//5n+I4EXL74ABBALxGSwagTjPzbAyMgItAQggBg9Pf9nZPx//x7kjL9////9C2QAyf9//qCQQCQkxFhY+BEggFi2b/+nq8v46BEDSPQ3w+8//3//BqFfv9BJeXmQEwACCOSkP38YgHy4Bog0RN0vIOMXVOTPH6Cv/gEEEEgDxFKgHEgDXCmGDUAE1AAQQCybGZg1f/d8//XsH0jTn3+///z79RtE/v4NZfz68xfI/vOX+4/0ZoZFAAHE4gYMvD+3/v2+h91wCANo9Z+/jH9VxBkYAAKIBRg9TL//MEhKAuWAogxgZzGC2CCfgUggAoYdGAEVAwQQ41egu5AQAyoXTQoIAAIMAD+JZR7YOGEWAAAAAElFTkSuQmCC" title="Русский" alt="Русский" width="16" height="11">
                                Русский
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</header>
