<footer class="page-footer font-small blue pt-4">

    <div id="container-contacts" class="container-fluid text-center">

        <div class="d-flex justify-content-center">
            <div class="row">
                <div class="col-sm-12">
                    <h2>
                        <?php echo translate("contacts-section-header")?>
                    </h2>

                    <a href="tel:+372 555 09 509">+372 555 09 509 EST</a>
                    <br/>
                    <a href="tel:+372 555 00 557">+372 555 00 557 RUS/ENG</a>
                    <br/>
                    <a href="mailto:info@getaway.zone">info@getaway.zone</a>
                    <p>
                        <a href="https://www.google.com/maps/place/GetAway+Zone+(escape+room+exit+room+games+Tallinn)/@59.436002,24.7445334,17z/data=!3m1!4b1!4m5!3m4!1s0x46929361f927df53:0x8ae8c353df877188!8m2!3d59.436002!4d24.7467221">Väike-Karja 1, floor 3, Tallinn 10140</a>
                    </p>
                </div>
            </div>
        </div>

    </div>

    <div id="container-social" class="d-flex flex-row justify-content-center">
        <div class="p-2 bd-highlight">
            <a href="https://www.facebook.com/getawayzone/" class="fb-ic">
                <i class="fab fa-facebook fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
            </a>
        </div>
        <div class="p-2 bd-highlight">
            <a href="https://www.instagram.com/getaway.zone/" class="ins-ic">
                <i class="fab fa-instagram fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
            </a>
        </div>
        <div class="p-2 bd-highlight">
            <a href="https://www.tripadvisor.com/Attraction_Review-g274958-d8720477-Reviews-Get_Away_Zone-Tallinn_Harju_County.html" class="ins-ic">
                <i class="fab fa-tripadvisor fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
            </a>
        </div>
    </div>

</footer>

<script>
    document.querySelectorAll('a[href^="#"]').forEach(anchor => {
        anchor.addEventListener('click', function (e) {
            e.preventDefault();

            document.querySelector(this.getAttribute('href')).scrollIntoView({
                behavior: 'smooth'
            });
        });
    });
</script>

<script src="assets/js/popper.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
