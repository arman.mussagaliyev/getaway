<?php

if (!function_exists("createOrder")) {
    function createOrder($mysqli, $game, $players, $dateTimeBooked, $price, $clientName, $clientEmail, $clientPhone) {
        $sql = "insert into gz_orders (game, players, date_time_booked, price, client_name, client_email, client_phone, state) values ('$game', $players, '$dateTimeBooked', $price, '$clientName', '$clientEmail', '$clientPhone', 'NEW')";
        return $mysqli->query($sql);
    }
}

if (!function_exists("getOrder")) {
    function getOrder($mysqli, $orderId)
    {
        return $mysqli->query("select * from gz_orders where id = $orderId")->fetch_assoc();
    }
}

if (!function_exists("getOrders")) {
    function getOrders($mysqli, $dateBookedFrom, $dateBookedTo, $game)
    {
        $gameClause = "1 = 1";
        if ($game !== "all") {
            $gameClause = "game='$game'";
        }
        $dateBookedTo .= " 23:59:59";
        return $mysqli->query("select * from gz_orders where $gameClause and date(date_time_booked) between '$dateBookedFrom' and '$dateBookedTo' order by date_time_booked desc limit 100");
    }
}

if (!function_exists("getActiveOrdersByGameAndDateTime")) {
    function getActiveOrdersByGameAndDateTime($mysqli, $game, $dateTimeBooked)
    {
        return $mysqli->query("select * from gz_orders where game = '$game' and date_time_booked = '$dateTimeBooked' and state in ('NEW', 'CONFIRMED')");
    }
}

if (!function_exists("confirmOrder")) {
    function confirmOrder($mysqli, $orderId)
    {
        return $mysqli->query("update gz_orders set state = 'CONFIRMED' where id={$orderId}");
    }
}

if (!function_exists("cancelOrder")) {
    function cancelOrder($mysqli, $orderId)
    {
        return $mysqli->query("update gz_orders set state = 'CANCELLED' where id={$orderId}");
    }
}

if (!function_exists("getBookedTimesByGameAndDate")) {
    function getBookedTimesByGameAndDate($mysqli, $game, $date)
    {
        return $mysqli->query("select date_format(date_time_booked, '%H:%i') as time_booked from gz_orders where game='$game' and date(date_time_booked) = '$date' and state in ('NEW', 'CONFIRMED')");
    }
}
?>
