<?php
    require "translation.php";
    require "db.php";
    require "orders-db.php";
    require "db-logger.php";
    require "notification.php";

    $game = $_POST["game"];
    $players = $_POST["players"];
    $dateBooked = $_POST["date_booked"];
    $timeBooked = $_POST["time_booked"];
    $dateTimeBooked = $dateBooked . " " . $timeBooked . ":00";
    $price = getPrice($game, $players);
    $clientName = $_POST["client_name"];
    $clientEmail = $_POST["client_email"];
    $clientPhone = $_POST["client_phone"];
    $orderSummary = "Game: $game\nPlayers: $players\nDate Time Booked: $dateTimeBooked\nPrice: $price\nClient Name: $clientName\nClient email: $clientEmail\nClient Phone: $clientPhone";
    $orderCreationResult = translate("could-not-book-message-on-screen");

    try {
        $mysqli = getConnection();

        if ($mysqli->connect_errno) {
            $errorMessage = $mysqli->connect_error;
            sendAdminNotification("Order Booking Failed [1]", "$errorMessage\nOrder Data:\n$orderSummary");
        } else {
            if (timeAvailable($mysqli, $game, $dateTimeBooked)) {
                $orderCreated = createOrder($mysqli, $game, $players, $dateTimeBooked, $price, $clientName, $clientEmail, $clientPhone);
                if ($orderCreated === false) {
                    $errorMessage = $mysqli->error;
                    sendAdminNotification("Order Booking Failed [2]", "$errorMessage\nOrder Data:\n$orderSummary");
                }
                else {
                    $orderId = $mysqli->insert_id;
                    $order = getOrder($mysqli, $orderId);
                    sendOrderNotification($mysqli, $orderId, "booking-received-subject", "booking-received-message");

                    $adminNotificationSubject = fillTemplateSubstitutes(translateToLang("order-created-subject", "en"), $order);
                    $adminNotificationMessage = fillTemplateSubstitutes(translateToLang("order-created-message", "en"), $order) . "\n" .
                        fillTemplateSubstitutes(translateToLang("order-admin-link", "en"), $order);
                    sendAdminNotification($adminNotificationSubject, $adminNotificationMessage);

                    $orderCreationResult = fillTemplateSubstitutes(translate("booking-received-message-on-screen"), $order);
                }
            } else {
                $orderCreationResult = translate("time-is-not-available-on-screen-message");
            }
        }
    } catch(Throwable | Error $e) {
        $errorMessage = $e->getMessage();
        $orderCreationResult = $errorMessage;
        logToDb($errorMessage . "\n" . $orderSummary);
        sendAdminNotification("Order Booking Failed [3]", "$errorMessage\nOrder Data:\n$orderSummary");
    } finally {
        $mysqli->close();
    }

    function timeAvailable($mysqli, $game, $dateTimeBooked) {
        try {
            $tz = new DateTimeZone("Europe/Tallinn");
            $now = new DateTime("now", $tz);
            $bookedDate = new DateTime($dateTimeBooked, $tz);
            if ($now < $bookedDate) {
                $diff = $now->diff($bookedDate);
                $minutes = $diff->days * 24 * 60 + $diff->h * 60 + $diff->i;
                if ($minutes < 60) {
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception $e) {
        }
        $order = getActiveOrdersByGameAndDateTime($mysqli, $game, $dateTimeBooked)->fetch_assoc();
        if ($order)
            return false;
        else
            return true;
    }

    function getPrice($game, $players) {
        if ($game == "red-alert") {
            if ($players == 2) {
                return 60;
            } elseif ($players == 3) {
                return 70;
            } elseif ($players == 4) {
                return 80;
            } elseif ($players == 5) {
                return 85;
            } elseif ($players == 6) {
                return 90;
            } elseif ($players == 7) {
                return 105;
            }
        }
        if ($game == "conjuring") {
            if ($players == 2) {
                return 60;
            } elseif ($players == 3) {
                return 70;
            } elseif ($players == 4) {
                return 80;
            } elseif ($players == 5) {
                return 85;
            } elseif ($players == 6) {
                return 90;
            } elseif ($players == 7) {
                return 105;
            }
        }
        return 105;
    }

    include "order-result.php";
?>
