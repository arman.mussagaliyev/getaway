<link rel="stylesheet" href="assets/css/booking.css">

<div id="container-booking" class="container-fluid text-center">
    <h2 id="book-now-header" class="text-center"><?php echo translate("book-now")?></h2>
    <input id="game" type="hidden" value="<?php echo $gameName?>">
    <div class="container container-calendar-wrapper container-calendar-wrapper-<?php echo $gameName?>">
        <table class="calendar">
            <tr>
                <td colspan="7">
                    <div class="month-header-elements">
                        <div class="month-header-element">
                            <button type="button" class="btn btn-sm btn-prev-month">
                                <img src="/assets/img/bootstrap-icons/chevron-left.svg" alt="">
                            </button>
                        </div>
                        <div class="month-header-element text-center month-header-element-month-name">
                            <p class="month text-center"></p>
                        </div>
                        <div class="month-header-element">
                            <button type="button" class="btn btn-sm btn-next-month">
                                <img src="/assets/img/bootstrap-icons/chevron-right.svg" alt="">
                            </button>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td><p id="week-day-1"></p></td>
                <td><p id="week-day-2"></p></td>
                <td><p id="week-day-3"></p></td>
                <td><p id="week-day-4"></p></td>
                <td><p id="week-day-5"></p></td>
                <td><p id="week-day-6"></p></td>
                <td><p id="week-day-0"></p></td>
            </tr>
        </table>
        <table class="players">
        </table>
        <table class="available-time">
        </table>
        <table class="book-button">
        </table>
    </div>
</div>

<script type="text/javascript" src="assets/js/pricing.js?v=<?php echo filemtime('assets/js/pricing.js'); ?>"></script>
<script type="text/javascript" src="assets/js/booking.js?v=<?php echo filemtime('assets/js/booking.js'); ?>"></script>

<script>
    booking.fillCalendar(booking.getCurrDate(), "<?php echo getCurrentLang()?>", "<?php echo $gameName?>");
    $(".btn-next-month").on("click", () => booking.fillCalendar(booking.getNextMonthFirstDate(booking.getCurrDate(), 1), "<?php echo getCurrentLang()?>", "<?php echo $gameName?>"));
    $(".btn-prev-month").on("click", () => booking.fillCalendar(booking.getNextMonthFirstDate(booking.getCurrDate(), -1), "<?php echo getCurrentLang()?>", "<?php echo $gameName?>"));
</script>
