<?php
    if (!function_exists("getConnection")) {
        function getConnection()
        {
            $params = parse_ini_file("../getaways-ENV.ini", true);

            $dbhost = $params["db"]["DBHOST"];
            $dbport = $params["db"]["DBPORT"];
            $dbname = $params["db"]["DBNAME"];
            $dbuser = $params["db"]["DBUSER"];
            $dbpass = $params["db"]["DBPASS"];

            $mysqli = new mysqli($dbhost, $dbuser, $dbpass, $dbname, $dbport);
            return $mysqli;
        }
    }
 ?>
