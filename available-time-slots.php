<?php
    require "db.php";
    require "orders-db.php";


    $mysqli = getConnection();

    $bookedDatesRes = getBookedTimesByGameAndDate($mysqli, $_GET["game"], $_GET["date"]);

    $availableTimes = array();
    while ($timeBooked = $bookedDatesRes->fetch_assoc()) {
        $availableTimes[] = $timeBooked["time_booked"];
    }

    $mysqli->close();

    echo json_encode($availableTimes);

?>
