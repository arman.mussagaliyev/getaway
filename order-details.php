<?php
    require "translation.php";
    require "db.php";
    require "orders-db.php";
    if (!isset($_GET["lang"])) {
        header("Location: /order-details.php?lang=en&orderId={$_GET["orderId"]}");
        exit();
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Google tag (gtag.js) --> <script async src="https://www.googletagmanager.com/gtag/js?id=AW-923155910"></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'AW-923155910'); </script>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/orders-details.css">

    <title>Get Away Zone | Orders Management</title>
</head>

<body>

    <?php
        $mysqli = getConnection();
        $order = getOrder($mysqli, $_GET["orderId"]);
        $mysqli->close();
        $orderStateText = translate("order-status-{$order["state"]}");
        $gameText = translate("game-{$order["game"]}");
    ?>

    <div class="d-flex justify-content-center">
        <form method="post" action="order-actions.php">
            <input type="hidden" id="orderId" name="orderId" value="<?php echo $order['id']?>">
            <div class="form-group">
                <label for="game"><?php echo translate("form-element-game")?></label>
                <input type="text" class="form-control form-control-sm" id="game" value="<?php echo $gameText?>" readonly>
            </div>
            <div class="form-group">
                <label for="players"><?php echo translate("form-element-players")?></label>
                <input type="text" class="form-control form-control-sm" id="players" value="<?php echo $order['players']?>" readonly>
            </div>
            <div class="form-group">
                <label for="dateBooked"><?php echo translate("form-element-date")?></label>
                <input type="text" class="form-control form-control-sm" id="dateBooked" value="<?php echo $order["date_time_booked"]?>" readonly>
            </div>
            <div class="form-group">
                <label for="price"><?php echo translate("form-element-price")?>, &euro;</label>
                <input type="text" class="form-control form-control-sm" id="price" value="<?php echo $order['price']?>" readonly>
            </div>
            <div class="form-group">
                <label for="clientName"><?php echo translate("form-element-name")?></label>
                <input type="text" class="form-control form-control-sm" id="clientName" value="<?php echo $order['client_name']?>" readonly>
            </div>
            <div class="form-group">
                <label for="clientEmail"><?php echo translate("form-element-email")?></label>
                <input type="email" class="form-control form-control-sm" name="clientEmail" id="clientEmail" value="<?php echo $order['client_email']?>" readonly>
            </div>
            <div class="form-group">
                <label for="clientPhone"><?php echo translate("form-element-phone")?></label>
                <input type="text" class="form-control form-control-sm" id="clientPhone" value="<?php echo $order['client_phone']?>" readonly>
            </div>
            <div class="form-group">
                <label for="state"><?php echo translate("form-element-status")?></label>
                <input type="text" class="form-control form-control-sm" id="state" value="<?php echo $orderStateText?>" readonly>
            </div>
            <?php
                if ($order["state"] === "NEW") {
                    $text = translate("btn-order-confirm");
                    echo "<button type='submit' class='btn btn-sm btn-success' name='btnConfirm' value='Y'>$text</button>";
                    $text = translate("btn-order-cancel");
                    echo "<button type='submit' class='btn btn-sm btn-danger float-right' name='btnCancel' value='Y'>$text</button>";
                }
                else if ($order["state"] === "CONFIRMED") {
                    $text = translate("btn-order-cancel");
                    echo "<button type='submit' class='btn btn-sm btn-danger only-button' name='btnCancel' value='Y'>$text</button>";
                }
                else if ($order["state"] === "CANCELLED") {
                    $text = translate("btn-order-confirm");
                    echo "<button type='submit' class='btn btn-sm btn-success only-button' name='btnConfirm' value='Y'>$text</button>";
                }
            ?>
            <a class='btn btn-sm btn-primary' href='orders.php' role='button'><?= translate("orders-list")?></a>
        </form>
    </div>

    <script src="assets/js/jquery-3.4.1.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
</body>
</html>
