<?php
    require "translation.php";
    require "db.php";
    require "orders-db.php";
    require "notification.php";

    $mysqli = getConnection();
    $orderId = $_POST["orderId"];
    $orderEmail = getOrder($mysqli, $orderId)["client_email"];
    $clientEmail = $_POST["clientEmail"];

    if ($orderEmail === $clientEmail) {
        if (isset($_POST["btnConfirm"]) && $_POST["btnConfirm"] === "Y") {
            confirmOrder($mysqli, $orderId);
            sendOrderNotification($mysqli, $orderId, "booking-confirmed-subject", "booking-confirmed-message");
        } elseif (isset($_POST["btnCancel"]) && $_POST["btnCancel"] === "Y") {
            cancelOrder($mysqli, $orderId);
            sendOrderNotification($mysqli, $orderId, "booking-cancelled-subject", "booking-cancelled-message");
        }
    }

    $mysqli->close();

    if (isset($_POST["cancelOrderYouself"]) && $_POST["cancelOrderYouself"] === "Y") {
        header("Location: /order-cancel.php?orderId=$orderId&clientEmail=$clientEmail");
    } else {
        header("Location: /orders.php");
    }
    exit();

?>
