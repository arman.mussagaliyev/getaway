<?php
    require "translation.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Google tag (gtag.js) --> <script async src="https://www.googletagmanager.com/gtag/js?id=AW-923155910"></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'AW-923155910'); </script>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/index.css">
	<link rel="stylesheet" href="assets/css/header.css">
	<link rel="stylesheet" href="assets/css/footer.css">
    <link rel="stylesheet" href="assets/css/brands.min.css">
    <link rel="stylesheet" href="assets/css/solid.min.css">
    <link rel="stylesheet" href="assets/css/fontawesome.min.css">
    <link rel="stylesheet" href="assets/css/checkout.css">

    <title>Get Away Zone | Escape Room</title>

    <!-- Hotjar Tracking Code for GetAwayZone -->
    <script>
        (function(h,o,t,j,a,r) {
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:3475847,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,"https://static.hotjar.com/c/hotjar-",".js?sv=");
    </script>
</head>

<body>

    <?php include "header.php"?>

    <div class="container-game container-game-<?php echo $_GET['game']?>" class="container-fluid">
    </div>

    <div class="d-flex container-checkout justify-content-center">

        <form method="post" action="create-order.php?lang=<?php echo $_GET['lang']?>">
            <input type="hidden" name="game" value="<?php echo $_GET['game']?>" readonly>
            <div class="form-group row">
                <label class="control-label col-sm-2" for="game"><?php echo translate("form-element-game")?></label>
                <div class="col-sm-10">
                    <input type="text" class="form-control-transparent form-control form-control-sm" id="game" value="<?php echo translate("game-" . $_GET['game'])?>" readonly="readonly">
                </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-sm-2" for="players"><?php echo translate("form-element-players")?></label>
                <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm" name="players" id="players" value="<?php echo $_GET['players']?>" readonly>
                </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-sm-2" for="date_booked"><?php echo translate("form-element-date")?></label>
                <div class="col-sm-10">
                    <input type="date" class="form-control form-control-sm" name="date_booked" id="date_booked" value="<?php echo $_GET["date"]?>" readonly>
                </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-sm-2" for="time_booked"><?php echo translate("form-element-time")?></label>
                <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm" name="time_booked" id="time_booked" value="<?php echo $_GET['time']?>" readonly>
                </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-sm-2" for="price"><?php echo translate("form-element-price")?>, &euro;</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm" id="price" value="<?php echo $_GET['price']?>" readonly>
                </div>
            </div>
            <div class="form-group">
                <input type="text" class="form-control form-control-sm" name="client_name" id="client_name" placeholder="<?php echo translate("placeholder-name")?>" required>
            </div>
            <div class="for-group row">
                <div class="col-sm-6">
                    <input type="email" class="form-control form-control-sm" name="client_email" id="client_email" placeholder="<?php echo translate("placeholder-email")?>" required>
                </div>
                <div class="col-sm-6">
                    <input type="text" class="form-control form-control-sm" name="client_phone" id="client_phone" placeholder="<?php echo translate("placeholder-phone")?>">
                </div>
            </div>
            <div class="for-group button-section">
                <button type="submit" class="btn btn-confirm btn-confirm-<?php echo $_GET['game']?>"><?php echo translate("btn-confirm")?></button>
            </div>
        </form>

    </div>

    <?php include "footer.php"?>

</body>

</html>
