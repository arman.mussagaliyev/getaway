<?php
    require "translation.php";
    require "db.php";
    require "orders-db.php";

    if (!isset($_GET["lang"])) {
        header("Location: /time-slot-management.php?lang=en");
        exit();
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Google tag (gtag.js) --> <script async src="https://www.googletagmanager.com/gtag/js?id=AW-923155910"></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'AW-923155910'); </script>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/orders.css">
    <link rel="stylesheet" href="assets/css/time-slots.css">

    <title>Get Away Zone | Orders Management</title>
</head>

<body>
    <form action="block.php">
        <div class="form-group">
            <label for="date-slot">Date</label>
            <input type="date" id="date-slot" class="form-control" value="<?php echo date("Y-m-d");?>" name="date-slot">
        </div>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="blockOption" id="cbWhole" value="whole">
            <label class="form-check-label" for="cbWhole">
                Whole day
            </label>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="blockOption" id="cbSelected" value="selected">
            <label class="form-check-label" for="cbSelected">
                Selected slots
            </label>
        </div>

        <div class="row time-slots-all">
            <div class="col-sm time-slots" id="time-slots-red-alert">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="red-alert-10-00" name="red-alert-10-00" value="on">
                    <label class="form-check-label" for="red-alert-10-00">
                        10:00
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="red-alert-11-20" name="red-alert-11-20" value="on">
                    <label class="form-check-label" for="red-alert-11-20">
                        11:20
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="red-alert-12-40" name="red-alert-12-40" value="on">
                    <label class="form-check-label" for="red-alert-12-40">
                        12:40
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="red-alert-14-00" name="red-alert-14-00" value="on">
                    <label class="form-check-label" for="red-alert-14-00">
                        14:00
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="red-alert-15-20" name="red-alert-15-20" value="on">
                    <label class="form-check-label" for="red-alert-15-20">
                        15:20
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="red-alert-16-40" name="red-alert-16-40" value="on">
                    <label class="form-check-label" for="red-alert-16-40">
                        16:40
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="red-alert-18-00" name="red-alert-18-00" value="on">
                    <label class="form-check-label" for="red-alert-18-00">
                        18:00
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="red-alert-19-20" name="red-alert-19-20" value="on">
                    <label class="form-check-label" for="red-alert-19-20">
                        19:20
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="red-alert-20-40" name="red-alert-20-40" value="on">
                    <label class="form-check-label" for="red-alert-20-40">
                        20:40
                    </label>
                </div>
            </div>
            <div class="col-sm time-slots" id="time-slots-conjuring">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="conjuring-10-15" name="conjuring-10-15" value="on">
                    <label class="form-check-label" for="conjuring-10-15">
                        10:15
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="conjuring-11-35" name="conjuring-11-35" value="on">
                    <label class="form-check-label" for="conjuring-11-35">
                        11:35
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="conjuring-12-55" name="conjuring-12-55" value="on">
                    <label class="form-check-label" for="conjuring-12-55">
                        12:55
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="conjuring-14-15" name="conjuring-14-15" value="on">
                    <label class="form-check-label" for="conjuring-14-15">
                        14:15
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="conjuring-15-35" name="conjuring-15-35" value="on">
                    <label class="form-check-label" for="conjuring-15-35">
                        15:35
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="conjuring-16-55" name="conjuring-16-55" value="on">
                    <label class="form-check-label" for="conjuring-16-55">
                        16:55
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="conjuring-18-15" name="conjuring-18-15" value="on">
                    <label class="form-check-label" for="conjuring-18-15">
                        18:15
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="conjuring-19-35" name="conjuring-19-35" value="on">
                    <label class="form-check-label" for="conjuring-19-35">
                        19:35
                    </label>
                </div>
            </div>
        </div>

        <button type="submit">Block</button>
    </form>

    <script src="assets/js/jquery-3.4.1.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/time-slot-manager.js"></script>
    <script>
        $("input[name='blockOption']").change(timeSlotManager.onBlockOptionChange);
        //timeSlotManager.setDefaultDate();
    </script>
</body>
</html>
