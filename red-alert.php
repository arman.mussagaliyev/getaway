<?php
    require "translation.php";
    $gameName = "red-alert";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Google tag (gtag.js) --> <script async src="https://www.googletagmanager.com/gtag/js?id=AW-923155910"></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'AW-923155910'); </script>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/red-alert.css">
	<link rel="stylesheet" href="assets/css/header.css">
	<link rel="stylesheet" href="assets/css/footer.css">
    <link rel="stylesheet" href="assets/css/brands.min.css">
    <link rel="stylesheet" href="assets/css/solid.min.css">
    <link rel="stylesheet" href="assets/css/fontawesome.min.css">

    <title>Get Away Zone | Escape Room</title>

    <!-- Hotjar Tracking Code for GetAwayZone -->
    <script>
        (function(h,o,t,j,a,r) {
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:3475847,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,"https://static.hotjar.com/c/hotjar-",".js?sv=");
    </script>
</head>

<body>

    <?php include "header.php"?>

    <div id="container-game" class="container-fluid">
        <h1 class="text-center">Red Alert</h1>
    </div>

    <div id="container-game-description" class="container-fluid">
        <p class="text-justify"><?php echo translate("game-page-red-alert-description1")?></p>
        <p class="text-justify"><?php echo translate("game-page-red-alert-description2")?></p>
        <p class="text-justify"><?php echo translate("game-page-red-alert-description3")?></p>
    </div>

    <?php include "booking.php"?>

    <?php include "footer.php"?>

</body>

</html>
