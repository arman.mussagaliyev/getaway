<?php
    require "translation.php";
    require "db.php";
    require "orders-db.php";

    if (!isset($_GET["lang"])) {
        header("Location: /orders.php?lang=en&dateBookedFrom=${$_GET['dateBookedFrom']}&dateBookedTo=${$_GET['dateBookedTo']}&game=${$_GET['game']}");
        exit();
    }
    $game = $_GET["game"];
    if (!isset($game) || $game === "") {
        $game = "all";
    }

    $dateBookedFrom = $_GET["dateBookedFrom"];
    if (!isset($dateBookedFrom) || $dateBookedFrom === "") {
        $dateBookedFrom = (new DateTime())->format("Y-m-d");
    }
    $dateBookedTo = $_GET["dateBookedTo"];
    if (!isset($dateBookedTo) || $dateBookedTo === "") {
        $tmp = new DateTime(date("Y-m-d"));
        $tmp->add(new DateInterval('P1D'));
        $dateBookedTo = $tmp->format("Y-m-d");
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Google tag (gtag.js) --> <script async src="https://www.googletagmanager.com/gtag/js?id=AW-923155910"></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'AW-923155910'); </script>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/orders.css">

    <title>Get Away Zone | Orders Management</title>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <form class="form-inline my-2 my-lg-0" method="get" action="orders.php" id="ordersForm">
                <input type="hidden" name="lang" value="en"/>
                <select class="form-control mr-sm-2" name="game" id="game" onchange="orders.getOrders()">
                    <option value="all" <?php if($game == "all") {echo "selected";}?>>All</option>
                    <option value="red-alert" <?php if($game == "red-alert") {echo "selected";}?>>Red Alert</option>
                    <option value="conjuring" <?php if($game == "conjuring") {echo "selected";}?>>Conjuring</option>
                </select>
                <input type="date" class="form-control mr-sm-2" name="dateBookedFrom" id="dateBookedFrom" value="<?php echo $dateBookedFrom?>" onchange="orders.getOrders()"/>
                <input type="date" class="form-control mr-sm-2" name="dateBookedTo" id="dateBookedTo" value="<?php echo $dateBookedTo?>" onchange="orders.getOrders()"/>
            </form>
        </div>
    </nav>

    <table class="table table-striped">
        <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col"><?php echo translate("order-table-header-game")?></th>
                <th scope="col"><?php echo translate("order-table-header-players")?></th>
                <th scope="col"><?php echo translate("order-table-header-date_and_time")?></th>
                <th scope="col"><?php echo translate("order-table-header-status")?></th>
                <th scope="col"><?php echo translate("order-table-header-details")?></th>
            </tr>
        </thead>
        <tbody>
            <?php
                $mysqli = getConnection();
                $orders = getOrders($mysqli, $dateBookedFrom, $dateBookedTo, $game);
                $viewButtonText = translate("btn-order-view");
                $langParam = "lang={$_GET["lang"]}";

                if ($orders->num_rows == 0) {
                    echo '<tr>';
                        echo "<td colspan='6'>No Orders Found</td>";
                    echo '</tr>';
                } else {
                    while ($order = $orders->fetch_assoc()) {
                        $orderStateText = translate("order-status-{$order["state"]}");
                        $gameText = translate("game-{$order["game"]}");

                        if ($order["game"] === "conjuring") {
                            $gameBadgeClass = "badge-primary";
                        } elseif ($order["game"] === "red-alert") {
                            $gameBadgeClass = "badge-danger";
                        }

                        if ($order["state"] === "NEW") {
                            $statusBadgeClass = "badge-warning";
                        }
                        elseif ($order["state"] === "CONFIRMED") {
                            $statusBadgeClass = "badge-success";
                        }
                        elseif ($order["state"] === "CANCELLED") {
                            $statusBadgeClass = "badge-danger";
                        }
                        echo '<tr>';
                        echo '<th scope="row">' . $order["id"] . '</th>';
                        echo "<td><span class='badge {$gameBadgeClass}'>{$gameText}</span></td>";
                        echo "<td>{$order["players"]}</td>";
                        echo "<td>{$order["date_time_booked"]}</td>";
                        echo "<td><span class='badge badge-pill {$statusBadgeClass}'>{$orderStateText}</span></td>";
                        echo "<td><a class='btn btn-sm btn-primary' href='order-details.php?orderId={$order["id"]}&{$langParam}' role='button'>{$viewButtonText}</a></td>";
                        echo '</tr>';
                    }
                }

                $mysqli->close();
            ?>
            <tr>
            </tr>
        </tbody>
    </table>
    <script src="assets/js/jquery-3.4.1.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/orders.js"></script>
</body>
</html>
