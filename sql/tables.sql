create table gz_orders
(
	id int auto_increment,
	created datetime not null default now(),
	lang varchar(10) not null default 'et',
	game varchar(100) not null,
	players int not null,
	date_time_booked datetime not null,
	price double not null,
	client_name varchar(100) not null,
	client_email varchar(100) not null,
	client_phone varchar(100) not null,
	state varchar(30) not null,
	constraint gz_orders_pk
		primary key (id)
);

create index gz_orders_date_time_booked_index
	on gz_orders (date_time_booked);

create index gz_orders_game_idx
    on gz_orders (game);

create table gz_log
(
	id int auto_increment,
	log_date_time timestamp not null default current_timestamp,
	log_message longtext null,
	constraint gz_log_pk
		primary key (id)
);

create index gz_log_date
	on gz_log (log_date_time);

create table gz_prices
(
    id int auto_increment,
    game varchar(20) not null,
    day int,
    time int,
    players int,
    price int not null
);

# red-alert
    # day_default
        # time_default
            # players_default
                insert into gz_prices (game, day, time, players, price) values ('red-alert', null, null, null, 55);
            insert into gz_prices (game, day, time, players, price) values ('red-alert', null, null, 2, 55);
            insert into gz_prices (game, day, time, players, price) values ('red-alert', null, null, 3, 65);
            insert into gz_prices (game, day, time, players, price) values ('red-alert', null, null, 4, 70);
            insert into gz_prices (game, day, time, players, price) values ('red-alert', null, null, 5,75);
            insert into gz_prices (game, day, time, players, price) values ('red-alert', null, null, 6, 80);
        #time_10
            insert into gz_prices (game, day, time, players, price) values ('red-alert', null, 10, 2, 55);
            insert into gz_prices (game, day, time, players, price) values ('red-alert', null, 10, 3, 65);
            insert into gz_prices (game, day, time, players, price) values ('red-alert', null, 10, 4, 70);
            insert into gz_prices (game, day, time, players, price) values ('red-alert', null, 10, 5,75);
            insert into gz_prices (game, day, time, players, price) values ('red-alert', null, 10, 6, 80);
        #time_12
            insert into gz_prices (game, day, time, players, price) values ('red-alert', null, 10, 2, 55);
            insert into gz_prices (game, day, time, players, price) values ('red-alert', null, 10, 3, 65);
            insert into gz_prices (game, day, time, players, price) values ('red-alert', null, 10, 4, 70);
            insert into gz_prices (game, day, time, players, price) values ('red-alert', null, 10, 5,75);
            insert into gz_prices (game, day, time, players, price) values ('red-alert', null, 10, 6, 80);
        #time_14
            insert into gz_prices (game, day, time, players, price) values ('red-alert', null, 10, 2, 55);
            insert into gz_prices (game, day, time, players, price) values ('red-alert', null, 10, 3, 65);
            insert into gz_prices (game, day, time, players, price) values ('red-alert', null, 10, 4, 70);
            insert into gz_prices (game, day, time, players, price) values ('red-alert', null, 10, 5,75);
            insert into gz_prices (game, day, time, players, price) values ('red-alert', null, 10, 6, 80);
        #time_16
            insert into gz_prices (game, day, time, players, price) values ('red-alert', null, 10, 2, 55);
            insert into gz_prices (game, day, time, players, price) values ('red-alert', null, 10, 3, 65);
            insert into gz_prices (game, day, time, players, price) values ('red-alert', null, 10, 4, 70);
            insert into gz_prices (game, day, time, players, price) values ('red-alert', null, 10, 5,75);
            insert into gz_prices (game, day, time, players, price) values ('red-alert', null, 10, 6, 80);
        #time_18
            insert into gz_prices (game, day, time, players, price) values ('red-alert', null, 10, 2, 55);
            insert into gz_prices (game, day, time, players, price) values ('red-alert', null, 10, 3, 65);
            insert into gz_prices (game, day, time, players, price) values ('red-alert', null, 10, 4, 70);
            insert into gz_prices (game, day, time, players, price) values ('red-alert', null, 10, 5,75);
            insert into gz_prices (game, day, time, players, price) values ('red-alert', null, 10, 6, 80);
        #time_20
            insert into gz_prices (game, day, time, players, price) values ('red-alert', null, 10, 2, 55);
            insert into gz_prices (game, day, time, players, price) values ('red-alert', null, 10, 3, 65);
            insert into gz_prices (game, day, time, players, price) values ('red-alert', null, 10, 4, 70);
            insert into gz_prices (game, day, time, players, price) values ('red-alert', null, 10, 5,75);
            insert into gz_prices (game, day, time, players, price) values ('red-alert', null, 10, 6, 80);
        #time_22
            insert into gz_prices (game, day, time, players, price) values ('red-alert', null, 10, 2, 55);
            insert into gz_prices (game, day, time, players, price) values ('red-alert', null, 10, 3, 65);
            insert into gz_prices (game, day, time, players, price) values ('red-alert', null, 10, 4, 70);
            insert into gz_prices (game, day, time, players, price) values ('red-alert', null, 10, 5,75);
            insert into gz_prices (game, day, time, players, price) values ('red-alert', null, 10, 6, 80);
